const PlayerModel = require('../models/Player.js');
class PlayerController {
    getById(id) {
        return new Promise((resolve, reject) => {
            PlayerModel.findOne({id}, (err, player) => {
                if (err) reject(err);
                resolve(player);
            })
        });
    }
    async isRegister(id) {
        let player = await this.getById(id);
        return player ? true : false;
    }
    register(playerData) {
        return new Promise((resolve, reject) => {
            let player = new PlayerModel({
                id: playerData.id,
                login: `Player${playerData.id}`
            });
            player
                .save()
                .then(data => resolve('All done'))
                .catch(err => {console.log(err.errmsg);});
        })

    }
    changeBalanceById(id, moneyDiff) {
        console.log('Изменяю баланс', id, moneyDiff);
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $inc: { balance: moneyDiff } },
                function(err) {
                    if (err) reject(err);
                    resolve('All done');
                });
        })
    }
    async getBonus(id, bonus) {
        this.setBonusTimeStep(id);
        this.changeBalanceById(id, bonus);
    }
    setBonusTimeStep(id) {
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $set: { getBonusAt: new Date() } },
                function(err) {
                    if (err) reject(err);
                    resolve('All done');
                });
        })
    }
    changeJob(id, jobId) {
        //Меняем workId на jobId
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $set: { jobId: jobId } },
                function(err) {
                    if (err) reject(err);
                    resolve('All done');
                });
        })
    }
    changeJobDaysCount(id, diff) {
        //Увеличиваем jobDays на diff
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $inc: { jobDaysCount: diff } },
                function(err) {
                    if (err) reject(err);
                    resolve('All done');
                });
        })
    }
    changeJobDays(id, diff) {
        //Увеличиваем jobDays на diff
        console.log('Изменяю дни работы', id, diff);
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $inc: { jobDays: diff } },
                function(err) {
                    if (err) reject(err);
                    resolve('All done');
                });
        })
    }
    setNewWorkAt(id) {
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $set: { workAt: new Date() } },
                function(err) {
                    if (err) reject(err);
                    resolve('All done');
                });
        })
    }
}
module.exports = new PlayerController();
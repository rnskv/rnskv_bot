const playerController = require('../controllers/PlayerController.js')
const { getRandomInt } = require('../helpers/helper.js');
class ActionsController {
    async showProfile(message) {
        let userId = message.user_id;
        let user = await playerController.getById(userId);

        message.addText(`Здравствуйте, ваши данные: \n ID: ${user.id} \n Логин: ${user.login} \n Баланс: ${user.balance}`).send()
    }
    async giveBonus(message) {
        let userId = message.user_id;

        let user = await playerController.getById(userId);
        let {money, getBonusAt} = user;

        let reloadTime =  1000 * 60 * 2;
        let lastGetTime = getBonusAt.getTime();
        let currentGetTime = new Date().getTime();

        let diffTime = currentGetTime - lastGetTime;
        let canGetBonus = diffTime >= reloadTime;

        if (canGetBonus) {
            let bonus = getRandomInt(1, 100);
            message.addText(`Поздравляем, вы получаете бонус в размере ${bonus} рублей!`).send()
            playerController.getBonus(userId, bonus);
        } else {
            let seconds = Math.round((reloadTime - diffTime) / 1000);

            let sLeft = seconds % 60;
            let mLeft = Math.floor(seconds / 60);
            let hLeft = Math.floor(mLeft / 60);
            message.addText(`Я дам бонус через ${hLeft > 9 ? hLeft : '0' + hLeft}:${mLeft > 9 ? mLeft : '0' + mLeft}:${sLeft > 9 ? sLeft : '0' + sLeft}`).send()
        }

    };
    async joinJob(message) {
        let userId = message.user_id;
        let user = await playerController.getById(userId);
        let { jobId } = user;

        if (jobId === 0) {
            await playerController.changeJob(userId, 2);
            message.addText(`Вы устроились на работу 2`).send();
        } else {
            playerController.setNewWorkAt(userId);
            message.addText(`Для того что бы устроиться на новую работу, вы должны уволиться с текущей`).send();
        }
    }
    async leaveJob(message) {
        let userId = message.user_id;
        let user = await playerController.getById(userId);
        let { jobId } = user;

        if (jobId === 0) {
            message.addText(`Вы не можете уволиться, вы безработны!`).send();
        } else {
            await playerController.changeJob(userId, 0);
            message.addText(`Вы уволились с работы!`).send();
        }
    }
    async work(message) {
        let userId = message.user_id;

        let user = await playerController.getById(userId);
        let { jobDaysCount, jobId, jobDays, workAt } = user;
        if (jobId !== 0) {

            if (jobDays !== 0) {
                playerController.changeBalanceById(userId, 5);
                playerController.setNewWorkAt(userId);
                playerController.changeJobDays(userId, -1);
                playerController.changeJobDaysCount(userId, 1);
                message.addText(`Работаем в пустоту, заработано за день 5 рублей. Работ осталось ${jobDays - 1}`).send();
            } else {
                let reloadTime =  1000 * 10;
                let lastGetTime = workAt.getTime();
                let currentGetTime = new Date().getTime();

                let diffTime = currentGetTime - lastGetTime;
                let canWork = diffTime >= reloadTime;

                if (canWork) {
                    await playerController.changeJobDays(userId, 3);
                    this.work(message);
                } else {
                    let seconds = Math.round((reloadTime - diffTime) / 1000);

                    let sLeft = seconds % 60;
                    let mLeft = Math.floor(seconds / 60);
                    let hLeft = Math.floor(mLeft / 60);
                    message.addText(`Работа закончилась, новые заказы будут через ${hLeft > 9 ? hLeft : '0' + hLeft}:${mLeft > 9 ? mLeft : '0' + mLeft}:${sLeft > 9 ? sLeft : '0' + sLeft}`).send();
                }
            }

        } else {
            message.addText(`Для того что бы начать трудиться вам необходимо устроиться на работу [Устроиться {id работы}]`).send();
        }
    }
}
module.exports =  new ActionsController();
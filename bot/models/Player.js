const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('Player', new Schema({
    id: {
        type: Number,
        unique: true,
    },
    login: String,
    balance: {
        type: Number,
        default: 0,
    },
    getBonusAt: {
        type: Date,
        default: 0
    },

    jobDaysCount: {
        type: Number,
        default: 0
    },
    jobId: {
        type: Number,
        default: 0,
    },
    jobDays: {
        type: Number,
        default: 3
    },
    workAt: {
        type: Date,
        default: 0
    }


}));
const TOKEN = '09273f9c9954187ae1da28a8ee6500e6b32e91e815f5c89ce57a09da64bc38d68f76621cdfd0c3e276782';
const mongoose = require('mongoose');
mongoose.connect('mongodb://rnskv:Fortest1@ds115193.mlab.com:15193/rnskv_bot', {useNewUrlParser: true});
const VK = require('vk-node-sdk');
const Group = new VK.Group(TOKEN);

const playerController = require('./controllers/PlayerController.js')
const actionsController = require('./controllers/ActionsController.js');

Group.onMessage(async (message) => {
    let userId = message.user_id;
    let userIsRegister = await playerController.isRegister(userId);

    if (!userIsRegister) {
        await playerController.register({
            id: userId
        })
        message.addText(`Регистрация завершена! \n Команды:\nПрофиль\nБонус`).send()
    }

    if (~message.body.toLowerCase().indexOf("профиль")) {
        actionsController.showProfile(message);
        return;
    }
    if (~message.body.toLowerCase().indexOf("бонус")) {
        actionsController.giveBonus(message);
        return;
    }
    if (~message.body.toLowerCase().indexOf("работать")) {
        actionsController.work(message);
        return;
    }

    if (~message.body.toLowerCase().indexOf("устроиться на работу")) {
        actionsController.joinJob(message);
        return;
    }
    if (~message.body.toLowerCase().indexOf("уволиться")) {
        actionsController.leaveJob(message);
        return;
    }
});
